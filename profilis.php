<!DOCTYPE HTML>
<?php session_start();
?>

<html>
    <head>
        <title>Profile</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css" />
		<link rel="shortcut icon" type="image/ico" href="images/favicon.ico" />
    </head>
    <body class="landing">   
        <div id="page-wrapper">

            <!-- Header -->
            <header id="header">
                <!--<h1 id="logo"><a href="index.html">Landed</a></h1>-->
                <nav id="nav">
                    <ul>
                        <li><a href="user.php">Home</a></li>
                        <li>
                            <a href="nuotraukugalerija.php">Galerija</a>
                            
                        </li>
                        <li><a href="profilis.php">Profile</a></li>
                        <li><a href="userblogas.php" class="button special">BLOG'AS</a></li>
                    </ul>
                </nav>
            </header>

             <section id="three" class="spotlight style3 left">
                <span class="image fit main bottom"><img src="images/pic04.jpg" alt="" /></span>
				
                <div class="content profiliscentre">
                    <header>
                        <?php
                            include "profilio_duom.php";
                        ?>
                        <h2>My profile</h2>
                    </header>

                    <div class="floatas sutarpu">

                        <h5> Firstname: </h5>
                        <h5> Lastname: </h5>
                        <h5> Email: </h5>
                        <h5> Phone: </h5>
                        <h5> Fax: </h5>
                        <h5> Address: </h5>
                    </div>
                   
                    

                    <div class="floatas">
                        <h5> <?php echo $_SESSION['name'] ?> </h5>
                        <h5> <?php echo $_SESSION['lastname'] ?> </h5>
                        <h5> <?php echo $_SESSION['email'] ?> </h5>
                        <h5> <?php echo $_SESSION['phone'] ?> </h5>
                        <h5> <?php echo $_SESSION['fax'] ?> </h5>
                        <h5> <?php echo $_SESSION['address'] ?> </h5> <br>
                    </div>

                    <div class="clearfix"></div>









                    <ul class="actions">
                        <li><a href="profilio_pakeit.php" class="button">Update profile</a></li>
                    </ul>
                </div>
				
                
            </section>


            

            <!-- Footer -->
            <footer id="footer">
                <ul class="icons">
                    <li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
                    <li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
                    <li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
                    <li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
                    <li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
                    <li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
                </ul>
                <ul class="copyright">
                    <li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
                </ul>
            </footer>

        </div>

        <!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.scrolly.min.js"></script>
        <script src="assets/js/jquery.dropotron.min.js"></script>
        <script src="assets/js/jquery.scrollex.min.js"></script>
        <script src="assets/js/skel.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>

    </body>
</html>