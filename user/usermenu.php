<?php
function name_to_talk($name) {
                $length = strlen($name);
                $last_symbols = $name[$length-2] . $name[$length-1];
                $break_name = substr($name, 0, $length - 2);
                switch($last_symbols) {
                        case 'as': return ($break_name . 'ai'); break;
                        case 'us': return ($break_name . 'au'); break;
                        case 'is': return ($break_name . 'i'); break;
                        case 'ys': return ($break_name . 'y'); break;
                        default:
                                if($last_symbols[1] === 'ė') return ($break_name . $last_symbols[0] . 'e');
                                return $name;
                }
        }
       
        
        
        $new = $_SESSION['vardas'];
        $new = strtolower($new);
       
        $new = ucfirst($new);             
        
       
        $betkoksvardas = name_to_talk($new);
?>

<!DOCTYPE HTML>
<html>
    <head>
        <title>Usermenu</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css" />
		<link rel="shortcut icon" type="image/ico" href="images/favicon.ico" />
    </head>
    <body class="landing">   
        <div id="page-wrapper">

            <!-- Header -->
            <header id="header">
                <h1 id="logo"><a href="index.html">LogOut</a></h1>
                <nav id="nav">
                    <ul>
                        <li><a href="user.php">Home</a></li>
                        <li>
                            <a href="nuotraukugalerija.php">Galerija</a>
                            
                        </li>
                        <li><a href="profilis.php">Profile</a></li>
                        <li><a href="userblogas.php" class="button special">BLOG'AS</a></li>
                    </ul>
                </nav>
            </header>

            <!-- Banner -->
            <section id="banner">
                <div class="content">
                    <header>
                    <?php
                  echo "Labas, <br> ". $betkoksvardas;
                  ?>
                    </header>
                    <span class="image"><img src="images/pic01.jpg" alt="" /></span>
                </div>
                <a href="#four" class="goto-next scrolly">Next</a>
            </section>


            <!-- Four -->
            <section id="four" class="wrapper style1 special fade-up">
                <div class="container">
                    <header class="major">
                        <h2>Accumsan sed tempus adipiscing blandit</h2>
                        <p>Iaculis ac volutpat vis non enim gravida nisi faucibus posuere arcu consequat</p>
                    </header>
                    <div class="box alt">
                        <div class="row uniform">
                            <section class="4u 6u(medium) 12u$(xsmall)">
                                <span class="icon alt major fa-area-chart"></span>
                                <h3>Ipsum sed commodo</h3>
                                <p>Feugiat accumsan lorem eu ac lorem amet accumsan donec. Blandit orci porttitor.</p>
                            </section>
                            <section class="4u 6u$(medium) 12u$(xsmall)">
                                <span class="icon alt major fa-comment"></span>
                                <h3>Eleifend lorem ornare</h3>
                                <p>Feugiat accumsan lorem eu ac lorem amet accumsan donec. Blandit orci porttitor.</p>
                            </section>
                            <section class="4u$ 6u(medium) 12u$(xsmall)">
                                <span class="icon alt major fa-flask"></span>
                                <h3>Cubilia cep lobortis</h3>
                                <p>Feugiat accumsan lorem eu ac lorem amet accumsan donec. Blandit orci porttitor.</p>
                            </section>
                            <section class="4u 6u$(medium) 12u$(xsmall)">
                                <span class="icon alt major fa-paper-plane"></span>
                                <h3>Non semper interdum</h3>
                                <p>Feugiat accumsan lorem eu ac lorem amet accumsan donec. Blandit orci porttitor.</p>
                            </section>
                            <section class="4u 6u(medium) 12u$(xsmall)">
                                <span class="icon alt major fa-file"></span>
                                <h3>Odio laoreet accumsan</h3>
                                <p>Feugiat accumsan lorem eu ac lorem amet accumsan donec. Blandit orci porttitor.</p>
                            </section>
                            <section class="4u$ 6u$(medium) 12u$(xsmall)">
                                <span class="icon alt major fa-lock"></span>
                                <h3>Massa arcu accumsan</h3>
                                <p>Feugiat accumsan lorem eu ac lorem amet accumsan donec. Blandit orci porttitor.</p>
                            </section>
                        </div>
                    </div>
                    <footer class="major">
                        <ul class="actions">
                            <li><a href="#" class="button">Magna sed feugiat</a></li>
                        </ul>
                    </footer>
                </div>
            </section>

            <!-- Five -->
            <section id="five" class="wrapper style2 special fade">
                <div class="container">
                    <header>
                        <h2>Magna faucibus lorem diam</h2>
                        <p>Ante metus praesent faucibus ante integer id accumsan eleifend</p>
                    </header>
                    <form method="post" action="#" class="container 50%">
                        <div class="row uniform 50%">
                            <div class="8u 12u$(xsmall)"><input type="email" name="email" id="email" placeholder="Your Email Address" /></div>
                            <div class="4u$ 12u$(xsmall)"><input type="submit" value="Get Started" class="fit special" /></div>
                        </div>
                    </form>
                </div>
            </section>

            <!-- Footer -->
            <footer id="footer">
                <ul class="icons">
                    <li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
                    <li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
                    <li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
                    <li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
                    <li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
                    <li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
                </ul>
                <ul class="copyright">
                    <li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
                </ul>
            </footer>

        </div>

        <!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.scrolly.min.js"></script>
        <script src="assets/js/jquery.dropotron.min.js"></script>
        <script src="assets/js/jquery.scrollex.min.js"></script>
        <script src="assets/js/skel.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>

    </body>
</html>